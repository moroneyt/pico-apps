//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h" // comment out when running on Wokwi RP2040 emulator.
#include "pico/float.h"
#include "pico/double.h"

void singlePrecision(int n) { // function to calculate pi with single-precision (float)
    float a = 1;
    float b = 3;
    float top = 2;
    float temp = 0;
    float pi = 0;
    float halfpi = 1;
    float actualPie = 3.14159265359;

    for(int count = 0; count <= n; count++) { // calculates pie 100000 times using the Wallis product algorithm
      float frac1 = top/a;
      float frac2 = top/b;
      temp = frac1 * frac2;
      halfpi = halfpi * temp; // multiplies the product of the 2 fractions by the result of the previous equation
      top = top + 2;
      a = a + 2;
      b = b + 2;
    }

    pi = halfpi * 2;

    printf("The calculated value for PI, using single-precision (float) floating-point representation, is: %f\n", pi);

    double error = 0;
    if (pi < actualPie) {    // calculates the difference between the Pie calculated and the actualPie
      error = actualPie - pi;
    }
    else {
      error = pi - actualPie;
    }
    printf("Approximated Error for single-precision number: %f\n", error);
}

void doublePrecision(int n) { // function to calculate pi with double-precision (double)
    double a = 1;
    double b = 3;
    double top = 2;
    double temp = 0;
    double pi = 0;
    double halfpi = 1;
    double actualPie = 3.14159265359;

    for(int count = 0; count <= n; count++) { // calculates pie 100000 times using the Wallis product algorithm
      double frac1 = top/a;
      double frac2 = top/b;
      temp = frac1 * frac2;
      halfpi = halfpi * temp; // multiplies the product of the 2 fractions by the result of the previous equation
      top = top + 2;
      a = a + 2;
      b = b + 2;
    }

    pi = halfpi * 2;

    printf("The calculated value for PI, using double-precision (double) floating-point representation, is: %f\n", pi);

    double error = 0;
    if (pi < actualPie) {    // calculates the difference between the Pie calculated by the program and the actualPie
      error = actualPie - pi;
    }
    else {
      error = pi - actualPie;
    }
    printf("Approximated Error for double-precision number: %f\n", error);
}


int main() {

#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif

    int n = 100000;
    singlePrecision(n);
    doublePrecision(n);

    // Returning zero indicates everything went okay.
    return 0;
}
