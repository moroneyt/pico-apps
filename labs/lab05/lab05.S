#include "hardware/regs/addressmap.h"
#include "hardware/regs/m0plus.h"

.syntax unified                 @ Specify unified assembly syntax
.cpu    cortex-m0plus           @ Specify CPU type is Cortex M0+
.thumb                          @ Specify thumb assembly for RP2040
.global main_asm                @ Provide program starting address to the linker
.align 4                        @ Specify code alignment

.equ    SLEEP_TIME, 500         @ Specify the sleep time (in ms)
.equ    LED_GPIO_PIN, 25        @ Specify the pin that the LED is connected to
.equ    LED_GPIO_OUT, 1         @ Specify the direction of the GPIO pin
.equ    LED_VALUE_ON, 1         @ Specify the value that turns the LED "on"
.equ    LED_VALUE_OFF, 0        @ Specify the value that turns the LED "off"
.equ    SVC_ISR_OFFSET, 0x2C    @ The SVC is entry 11 in the vector table
.equ    SVC_MAX_INSTRS, 0x01    @ Maximum allowed SVC subroutines

@ Entry point to the ASM portion of the program
main_asm:
    bl      init_gpio_led       @ Initialise the GPIO LED pin
    bl      install_svc_isr     @ Install the SVC interrupt service routine
loop:
    svc     #0                  @ Call the SVC ISR with value 0 (turns on LED)
    nop                         @ Add a no-op instruction for alignment after SVC
    bl      do_sleep            @ Short pause before proceeding
    svc     #1                  @ Call the SVC ISR with value 1 (turns off LED)
    nop                         @ Add a no-op instruction for alignment after SVC
    bl      do_sleep            @ Add a short pause before proceeding
    b       loop                @ Always jump back to the start of the loop

@ Subroutine used to introduce a short delay in the application
do_sleep:
    ldr     r0, =SLEEP_TIME             @ Set the value of SLEEP_TIME we want to wait for
    bl      sleep_ms                    @ Sleep until SLEEP_TIME has elapsed then toggle the LED GPIO pin

@ Subroutine used to initialise the PI Pico built-in LED
init_gpio_led:
    movs    r0, #LED_GPIO_PIN           @ This value is the GPIO LED pin on the PI PICO board
    bl      asm_gpio_init               @ Call the subroutine to initialise the GPIO pin specified by r0
    movs    r0, #LED_GPIO_PIN           @ This value is the GPIO LED pin on the PI PICO board
    movs    r1, #LED_GPIO_OUT           @ We want this GPIO pin to be setup as an output pin
    bl      asm_gpio_set_dir            @ Call the subroutine to set the GPIO pin specified by r0 to state specified by r1

@ Subroutine used to install the SVC interrupt service handler
install_svc_isr:
    ldr     r2, =(PPB_BASE + M0PLUS_VTOR_OFFSET)    @ Load r2 with the address of the first entry of the cortex M0+ vector table
    ldr     r1, [r2]                                @ Load r1 with the value stored at the address in r2
    movs    r2, #SVC_ISR_OFFSET                     @ Load r2 with the constant SVC_ISR_OFFSET which corresponds to the 11th entry in the vector table
    add     r2, r1                                  @ Add the offset to the value in r1. r2 now contains the address of the SVC ISR in the vector table
    ldr     r0, =svc_isr                            @ Load r0 with the value stored in the memory space labeled "svc_isr"
    str     r0, [r2]                                @ Store the value in r0 at the address of r2 in memory, this activates the SVC ISR
    bx      lr                                      @ Exit the install_svc_isr subroutine

@ SVC interrupt service handler routine
.thumb_func                     @ Required for all interrupt service routines
svc_isr:
    push    {lr}                @ Push the link register to the stack as we will call nested subroutines
    ldr     r0, [sp, #0x1C]     @ Find the SVC instruction opcode that called the routine from the stack frame
    subs    r0, #0x2            @ Subtract 0x2 from the instruction and update the condition flags
    ldr     r0, [r0]            @ Load r0 with the value stored in memory at the address of the instruction
    ldr     r1, =#0xFF          @ Load r1 with the value 0xFF (this is a mask)
    ands    r0, r1              @ Bitwise AND the values in r0 and r1 to isolate the 8 bit comment field in the opcode stored in r0
    cmp     r0, #SVC_MAX_INSTRS @ Check if value in r0 is greater than 1 (Check if it's a valid instruction)
    bgt     svc_done            @ If r0 is greater than 1, exit the subroutine (not a valid instruction)
    adr     r1, svc_jmptbl      @ Load r1 with the address of the svc jumptable
    lsls    r0, #2              @ Logical shift left the value in r0 by two places, making it either 0 or 4
    ldr     r1, [r1, r0]        @ Load r1 with the value stored in memory at address [jumptable + r0]
    mov     pc, r1              @ Load the value in r1 into the program counter to jump to either svc_num0 or svc_num1
svc_done:
    pop     {pc}                @ Pop the link register from the stack to the program counter

@ First function of SVC subroutine - turn on the LED
svc_num0:
    push    {lr}                        @ Store the link register to the stack as we will call nested subroutines
    movs    r1, #LED_VALUE_ON           @ The LED is currently "off" so we want to turn it "on"
    movs    r0, #LED_GPIO_PIN           @ Set the LED GPIO pin number to r0 for use by asm_gpio_put
    bl      asm_gpio_put                @ Update the the value of the LED GPIO pin (based on value in r1)
    b       svc_done                    @ Branch back to the main ISR when done
    pop     {pc}                        @ Pop the link register from the stack to the program counter

@ Second function of SVC subroutine - turn off the LED
svc_num1:
    push    {lr}                        @ Store the link register to the stack as we will call nested subroutines
    movs    r1, #LED_VALUE_OFF          @ The LED is currently "off" so we want to turn it "on"
    movs    r0, #LED_GPIO_PIN           @ Set the LED GPIO pin number to r0 for use by asm_gpio_put
    bl      asm_gpio_put                @ Update the the value of the LED GPIO pin (based on value in r1)
    b       svc_done                    @ Branch back to the main ISR when done
    pop     {pc}                        @ Pop the link register from the stack to the program counter

@ SVC function entry jump table.
.align 2
svc_jmptbl:
    .word svc_num0              @ Entry zero goes to SVC function #0.
    .word svc_num1              @ Entry one goes to SVC function #1.
    .word 0                     @ Null termination of the jump table.

@ Set data alignment
.data
    .align 4